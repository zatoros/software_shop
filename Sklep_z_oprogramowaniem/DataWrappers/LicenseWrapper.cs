﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Controls;
using System.Windows;

namespace Sklep_z_oprogramowaniem.DataWrappers
{
    public class LicenseWrapper : RowHelper
    {
        private const string tableName = Names.TableNames.Licenses;

        private DataWindows.LicenseDataWindow DataWindow;

        public DataField<DateTime> StartDate = new DataField<DateTime>(Names.FieldNames.LicenceStart,
            Names.FriendlyFieldNames.LicenceStart);
        public DataField<DateTime> EndDate = new DataField<DateTime>(Names.FieldNames.LicenceEnd,
            Names.FriendlyFieldNames.LicenceEnd);

        public ContextField<long> RTransactionID = new ContextField<long>("RTransactionID", Names.TableNames.Transactions,
            Names.FieldNames.TransactionDescription, Names.FriendlyFieldNames.TransactionDescription);

        public override bool anyFieldUsed()
        {
            return StartDate.isUsed || EndDate.isUsed || RTransactionID.isUsed;
        }

        public override string prepareInsertQuery() 
        {
            if (anyFieldUsed())
            {
                string queryString = string.Format("INSERT INTO {0} ", tableName);

                queryString += Helper.getINTOfieldNames(StartDate.Name, EndDate.Name,
                    RTransactionID.Name);

                queryString += " VALUES ";

                queryString += Helper.getINTOfieldNames(Helper.getWithQuote(StartDate.Value.ToString()),
                    Helper.getWithQuote(EndDate.Value.ToString()), RTransactionID.Value.ToString());

                return queryString;
            }
            else
            {
                throw new Exception(Names.ExceptionNames.NoUsedFieldsInsert);
            }
        }

        public override string prepareUpdateQuery() 
        {
            if (anyFieldUsed())
            {
                string queryString = string.Format("UPDATE {0} SET ", tableName);

                queryString += Helper.getSET(StartDate, true);
                queryString += Helper.getSET(EndDate, true);
                queryString += Helper.getSET(RTransactionID, false, false);

                return queryString;
            }
            else
            {
                throw new Exception(Names.ExceptionNames.NoUsedFieldsUpdate);
            }
        }
        
        public override string prepereSelectQuery()
        {
            string queryString = string.Empty;

            if (anyFieldUsed())
            {
                queryString = string.Format("SELECT {2}.{0} AS {1} ", Id.Name, Id.FriendlyName,
                    tableName);

                queryString += Helper.getASfield(StartDate, tableName);
                queryString += Helper.getASfield(EndDate, tableName);

                queryString += Helper.getASfield(RTransactionID, tableName);

                queryString += Helper.getASCOntextField(RTransactionID);

                queryString += string.Format(" FROM {0}", TableName);

                queryString += Helper.getJOIN(RTransactionID, TableName);
            }
            else
            {
                throw new Exception(Names.ExceptionNames.NoUsedFieldsSelect);
            }

            return queryString;
        }

        public override void useAllFields()
        {
            StartDate.isUsed = true;
            EndDate.isUsed = true;

            RTransactionID.isUsed = true;
        }

        public override void unUseAllFields()
        {
            StartDate.isUsed = true;
            EndDate.isUsed = true;

            RTransactionID.isUsed = true;
        }

        public override string TableName
        {
            get { return tableName; }
        }

        public override bool? LoadObject(DataWindows.Helper.WindowType operationType,
            DataWrapper dataWrapper, SqlConnection sqlCon)
        {

            DataWindow = new DataWindows.LicenseDataWindow(operationType, this, dataWrapper, sqlCon);
            return DataWindow.ShowDialog();

        }
        
        public override void GetObjectFromRow(DataRowView dataRowView) 
        {
            Id.Value = long.Parse(dataRowView[0].ToString());

            StartDate.Value = DateTime.Parse(dataRowView[1].ToString());
            EndDate.Value = DateTime.Parse(dataRowView[2].ToString());

            RTransactionID.Value = long.Parse(dataRowView[3].ToString());
        }

        public override void HideIdRows(DataGrid grid)
        {
            base.HideIdRows(grid);

            grid.Columns[3].Visibility = Visibility.Hidden;
        }
    }
}
