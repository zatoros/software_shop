﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Controls;
using System.Windows;

namespace Sklep_z_oprogramowaniem.DataWrappers 
{
    public class ClientWrapper : RowHelper
    {
        private const string tableName = Names.TableNames.Clients;

        private DataWindows.ClientDataWindow DataWindow;

        public DataField<string> Name = new DataField<string>(Names.FieldNames.ClientName,
            Names.FriendlyFieldNames.ClientName);
        public DataField<string> Phone = new DataField<string>(Names.FieldNames.ClientPhone,
            Names.FriendlyFieldNames.ClientPhone);
        public DataField<string> Address = new DataField<string>(Names.FieldNames.ClientAddress,
            Names.FriendlyFieldNames.ClientAddress);

        public ContextField<long> RClientStatusID = new ContextField<long>("RClientStatusID", Names.TableNames.ClientStatus,
            Names.FieldNames.ClientStatus, Names.FriendlyFieldNames.ClientStatus);
        public ContextField<long> RWorkerID = new ContextField<long>("RWorkerID", Names.TableNames.Workers,
            Names.FieldNames.WorkerName, Names.FriendlyFieldNames.WorkerName);

        public override bool anyFieldUsed()
        {
            return Name.isUsed || Address.isUsed || Phone.isUsed ||
                RClientStatusID.isUsed || RWorkerID.isUsed;
        }

        public override string prepareInsertQuery()
        {
            if (anyFieldUsed())
            {
                string queryString = string.Format("INSERT INTO {0} ", tableName);

                queryString += Helper.getINTOfieldNames(Name.Name, Address.Name,
                    Phone.Name, RClientStatusID.Name, RWorkerID.Name);

                queryString += " VALUES ";

                queryString += Helper.getINTOfieldNames(Helper.getWithQuote(Name.Value),
                    Address.Value != "" ? Helper.getWithQuote(Address.Value) : "NULL",
                     Helper.getWithQuote(Phone.Value),
                     RClientStatusID.Value != 0 ? RClientStatusID.Value.ToString() : "NULL",
                     RWorkerID.Value != 0 ? RWorkerID.Value.ToString() : "NULL");

                return queryString;
            }
            else
            {
                throw new Exception(Names.ExceptionNames.NoUsedFieldsInsert);
            }
        }
        
        public override string prepareUpdateQuery() 
        {
            if (anyFieldUsed())
            {
                string queryString = string.Format("UPDATE {0} SET ", tableName);

                queryString += Helper.getSET(Name, true);
                queryString += Address.Value != "" ? Helper.getSET(Address, true) : Helper.getSETnull(Address);
                queryString += Helper.getSET(Phone, true);

                queryString += RClientStatusID.Value != 0 ? Helper.getSET(RClientStatusID) : Helper.getSETnull(RClientStatusID);
                queryString += RWorkerID.Value != 0 ? Helper.getSET(RWorkerID,false,false) : Helper.getSETnull(RWorkerID,false);

                queryString += Helper.getWHEREId(Id);

                return queryString;
            }
            else
            {
                throw new Exception(Names.ExceptionNames.NoUsedFieldsUpdate);
            }
        }

        public override string prepereSelectQuery()
        {
            string queryString = string.Empty;

            if (anyFieldUsed())
            {
                queryString = string.Format("SELECT {2}.{0} AS {1} ", Id.Name, Id.FriendlyName,
                    tableName);

                queryString += Helper.getASfield(Name,tableName);
                queryString += Helper.getASfield(Address,tableName);
                queryString += Helper.getASfield(Phone, tableName);

                queryString += Helper.getASfield(RClientStatusID, tableName);
                queryString += Helper.getASfield(RWorkerID, tableName);

                queryString += Helper.getASCOntextField(RClientStatusID);
                queryString += Helper.getASCOntextField(RWorkerID);

                queryString += string.Format(" FROM {0}", TableName);

                queryString += Helper.getJOIN(RClientStatusID, TableName);
                queryString += Helper.getJOIN(RWorkerID, TableName);
            }
            else
            {
                throw new Exception(Names.ExceptionNames.NoUsedFieldsSelect);
            }

            return queryString;
        }

        public override void useAllFields()
        {
            Name.isUsed = true;
            Address.isUsed = true;
            Phone.isUsed = true;

            RClientStatusID.isUsed = true;
            RWorkerID.isUsed = true;
        }

        public override void unUseAllFields()
        {
            Name.isUsed = false;
            Address.isUsed = false;
            Phone.isUsed = false;

            RClientStatusID.isUsed = false;
            RWorkerID.isUsed = false;
        }

        public override string TableName
        {
            get { return tableName; }
        }

        public override bool? LoadObject(DataWindows.Helper.WindowType operationType, DataWrapper dataWrapper, SqlConnection sqlCon)
        {
            DataWindow = new DataWindows.ClientDataWindow(operationType, this, dataWrapper, sqlCon);
            return DataWindow.ShowDialog();
        }
        
        public override void GetObjectFromRow(DataRowView dataRowView) 
        {
            Id.Value = long.Parse(dataRowView[0].ToString());

            Name.Value = dataRowView[1].ToString();
            Address.Value = dataRowView[2].ToString();
            Phone.Value = dataRowView[3].ToString();

            RClientStatusID.Value = long.Parse(string.IsNullOrEmpty(dataRowView[4].ToString()) 
                ? "0" : dataRowView[4].ToString());
            RWorkerID.Value = long.Parse(string.IsNullOrEmpty(dataRowView[5].ToString()) 
                ? "0" : dataRowView[5].ToString());

            RClientStatusID.ContextFieldValue = dataRowView[6].ToString();
            RWorkerID.ContextFieldValue = dataRowView[7].ToString();
        }

        public override void HideIdRows(DataGrid grid)
        {
            base.HideIdRows(grid);

            grid.Columns[4].Visibility = Visibility.Hidden;
            grid.Columns[5].Visibility = Visibility.Hidden;
        }
    }
}
