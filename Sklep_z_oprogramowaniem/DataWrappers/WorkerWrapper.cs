﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Controls;
using System.Windows;

namespace Sklep_z_oprogramowaniem.DataWrappers
{
    public class WorkerWrapper : RowHelper
    {
        private const string tableName = Names.TableNames.Workers;

        private DataWindows.WorkerDataWindow DataWindow;

        public DataField<string> Name = new DataField<string>(Names.FieldNames.WorkerName,
            Names.FriendlyFieldNames.WorkerName);
        public DataField<string> Surname = new DataField<string>(Names.FieldNames.WorkerSurname,
            Names.FriendlyFieldNames.WorkerSurname);
        public DataField<decimal> Salary = new DataField<decimal>(Names.FieldNames.WorkerSalary,
            Names.FriendlyFieldNames.WorkerSalary);
        public DataField<string> Address = new DataField<string>(Names.FieldNames.WorkerAddress,
            Names.FriendlyFieldNames.WorkerAddress);
        public DataField<string> Phone = new DataField<string>(Names.FieldNames.WorkerPhone,
            Names.FriendlyFieldNames.WorkerPhone);


        public ContextField<long> RWorkTimeID = new ContextField<long>("RWorkTimeID",Names.TableNames.WorkTime,
            Names.FieldNames.WorkTimeType, Names.FriendlyFieldNames.WorkTimeType);
        public ContextField<long> RPositionID = new ContextField<long>("RPositionID", Names.TableNames.Position,
            Names.FieldNames.PositionName, Names.FriendlyFieldNames.PositionName);

        public override bool anyFieldUsed()
        {
            return Name.isUsed || Surname.isUsed || Salary.isUsed
                || Address.isUsed || Phone.isUsed ||
                RWorkTimeID.isUsed || RPositionID.isUsed;
        }

        public override string prepareInsertQuery() 
        {
            if (anyFieldUsed())
            {
                string queryString = string.Format("INSERT INTO {0} ", tableName);

                queryString += Helper.getINTOfieldNames(Name.Name, Surname.Name,
                    Salary.Name, Address.Name, Phone.Name, RWorkTimeID.Name, RPositionID.Name);

                queryString += " VALUES ";

                queryString += Helper.getINTOfieldNames(Helper.getWithQuote(Name.Value), Helper.getWithQuote(Surname.Value),
                    Salary.Value.ToString().Replace(',','.'),
                    Address.Value != "" ? Helper.getWithQuote(Address.Value) : "NULL", Helper.getWithQuote(Phone.Value),
                    RWorkTimeID.Value.ToString(), RPositionID.Value.ToString());

                return queryString;
            }
            else
            {
                throw new Exception(Names.ExceptionNames.NoUsedFieldsInsert);
            }
        }

        public override string prepareUpdateQuery() 
        {
            if (anyFieldUsed())
            {
                string queryString = string.Format("UPDATE {0} SET ", tableName);

                queryString += Helper.getSET(Name, true);
                queryString += Helper.getSET(Surname, true);
                queryString += Helper.getSET(Salary);
                queryString += Address.Value != "" ? Helper.getSET(Address, true) : Helper.getSETnull(Address);
                queryString += Helper.getSET(Phone, true);
                queryString += Helper.getSET(RWorkTimeID);
                queryString += Helper.getSET(RPositionID, false, false);

                queryString += Helper.getWHEREId(Id);

                return queryString;
            }
            else
            {
                throw new Exception(Names.ExceptionNames.NoUsedFieldsUpdate);
            }
        }
        
        public override string prepereSelectQuery()
        {
            string queryString = string.Empty;

            if (anyFieldUsed())
            {
                queryString = string.Format("SELECT {2}.{0} AS {1} ", Id.Name, Id.FriendlyName, 
                    tableName);

                queryString += Helper.getASfield(Name, tableName);
                queryString += Helper.getASfield(Surname, tableName);
                queryString += Helper.getASfield(Salary,tableName);
                queryString += Helper.getASfield(Address, tableName);
                queryString += Helper.getASfield(Phone, tableName);

                queryString += Helper.getASfield(RWorkTimeID, tableName);
                queryString += Helper.getASfield(RPositionID, tableName);

                queryString += Helper.getASCOntextField(RWorkTimeID); 
                queryString += Helper.getASCOntextField(RPositionID);

                queryString += string.Format(" FROM {0}", TableName);

                queryString += Helper.getJOIN(RWorkTimeID, TableName);
                queryString += Helper.getJOIN(RPositionID, TableName);
            }
            else
            {
                throw new Exception(Names.ExceptionNames.NoUsedFieldsSelect);
            }

            return queryString;
        }

        public override void useAllFields()
        {
            Name.isUsed = true;
            Surname.isUsed = true;
            Salary.isUsed = true;
            Address.isUsed = true;
            Phone.isUsed = true;

            RWorkTimeID.isUsed = true;
            RPositionID.isUsed = true;
        }

        public override void unUseAllFields()
        {
            Name.isUsed = false;
            Surname.isUsed = false;
            Salary.isUsed = false;
            Address.isUsed = false;
            Phone.isUsed = false;

            RWorkTimeID.isUsed = false;
            RPositionID.isUsed = false;
        }

        public override string TableName
        {
            get { return tableName; }
        }

        public override bool? LoadObject(DataWindows.Helper.WindowType operationType, DataWrapper dataWrapper, 
            SqlConnection sqlCon) 
        {
            DataWindow = new DataWindows.WorkerDataWindow(operationType, this, dataWrapper, sqlCon);
            return DataWindow.ShowDialog();
        }

        public override void GetObjectFromRow(DataRowView dataRowView) 
        {
            Id.Value = long.Parse(dataRowView[0].ToString());

            Name.Value = dataRowView[1].ToString();
            Surname.Value = dataRowView[2].ToString();
            Salary.Value = decimal.Parse(dataRowView[3].ToString());
            Address.Value = dataRowView[4].ToString();
            Phone.Value = dataRowView[5].ToString();

            RWorkTimeID.Value = long.Parse(dataRowView[6].ToString());
            RPositionID.Value = long.Parse(dataRowView[7].ToString());
        }

        public override void HideIdRows(DataGrid grid)
        {
            base.HideIdRows(grid);

            grid.Columns[6].Visibility = Visibility.Hidden;
            grid.Columns[7].Visibility = Visibility.Hidden;
        }
    }
}
