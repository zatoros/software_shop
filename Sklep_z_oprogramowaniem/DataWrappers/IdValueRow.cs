﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace Sklep_z_oprogramowaniem.DataWrappers
{
    public abstract class IdValueRow : RowHelper
    {
        public abstract DataField<string> Value { set; get; }

        private DataWindows.IdValueRowDataWindow DataWindow;

        public override void useAllFields()
        {
            Value.isUsed = true;
        }

        public override void unUseAllFields()
        {
            Value.isUsed = false;
        }

        public override bool anyFieldUsed()
        {
            return Value.isUsed;
        }

        public override string prepareInsertQuery()
        {
            if (Value.isUsed)
            {
                return string.Format("INSERT INTO {0} ({1}) VALUES ('{2}')",
                    TableName, Value.Name, Value.Value);
            }
            else
            {
                throw new Exception(string.Format("Błąd przy dodawaniu rekordu:" +
                   "nie podano wartości pola {0}",Value.Name));
            }
        }

        public override string prepareDeleteQuery()
        {
            if (Value.isUsed)
            {
                return string.Format("DELETE FROM {0} WHERE {1} = {2}",
                    TableName, Id.Name, Id.Value);
            }
            else
            {
                throw new Exception(string.Format("Błąd przy usuwaniu rekordu:" +
                    "nie podano wartości pola {0}", Value.Name));
            }
        }

        public override string prepareUpdateQuery()
        {
            if (Value.isUsed)
            {
                return string.Format("UPDATE {0} SET {1} = '{2}' WHERE {3} = {4}",
                    TableName, Value.Name, Value.Value, Id.Name, Id.Value);
            }
            else
            {
                throw new Exception(string.Format("Błąd przy uaktualnianiu rekordu:" +
                    "nie podano wartości pola {0}", Value.Name));
            }
        }

        public override string prepereSelectQuery()
        {
            if (Value.isUsed)
            {
                return string.Format("SELECT {0} AS {1}, {2} AS {3} FROM {4}",
                    Id.Name, Id.FriendlyName, Value.Name, Value.FriendlyName, TableName);
            }
            else
            {
                throw new Exception(string.Format("Błąd przy ładowaniu rekordów:" +
                    "nie podano wartości pola {0}", Value.Name));
            }
        }

        public override bool? LoadObject(DataWindows.Helper.WindowType operationType, DataWrapper dataWrapper, SqlConnection sqlCon)
        {
            DataWindow = new DataWindows.IdValueRowDataWindow(operationType, this);
            return DataWindow.ShowDialog();
        }

        public override void GetObjectFromRow(DataRowView dataRowView) 
        {
            Id.Value = long.Parse(dataRowView[0].ToString());
            Value.Value = dataRowView[1].ToString();
        }
    }
}
