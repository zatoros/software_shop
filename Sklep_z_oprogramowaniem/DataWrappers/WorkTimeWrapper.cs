﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sklep_z_oprogramowaniem.DataWrappers
{
    class WorkTimeWrapper : IdValueRow
    {
        private const string tableName = Names.TableNames.WorkTime;
        private DataField<string> workTimeType = new DataField<string>(Names.FieldNames.WorkTimeType,
            Names.FriendlyFieldNames.WorkTimeType);

        public override DataField<string> Value
        {
            get
            {
                return workTimeType;
            }
            set
            {
                workTimeType = value;
            }
        }

        public override string TableName
        {
            get { return tableName; }
        }
    }
}
