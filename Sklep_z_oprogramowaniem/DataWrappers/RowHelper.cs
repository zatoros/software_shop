﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.Windows.Controls;
using System.Windows;

namespace Sklep_z_oprogramowaniem.DataWrappers
{
    public abstract class RowHelper
    {
        public abstract string TableName { get; }
        public DataField<long> Id = new DataField<long>(Names.FieldNames.Id);

        abstract public bool anyFieldUsed();

        abstract public string prepareInsertQuery();
        
        virtual public string prepareDeleteQuery()
        {
            if (anyFieldUsed())
            {
                return string.Format("DELETE FROM {0} WHERE {1} = {2}",
                    TableName, Id.Name, Id.Value);
            }
            else
            {
                throw new Exception(Names.ExceptionNames.NoUsedFieldsDelete);
            }
        }

        virtual public void HideIdRows(DataGrid grid)
        {
            grid.Columns[0].Visibility = Visibility.Hidden;
        }

        abstract public string prepareUpdateQuery();
        abstract public string prepereSelectQuery();

        abstract public void useAllFields();
        abstract public void unUseAllFields();

        abstract public bool? LoadObject(DataWindows.Helper.WindowType operationType, DataWrapper dataWrapper, SqlConnection sqlCon);
        abstract public void GetObjectFromRow(DataRowView dataRowView);

        public DataTable Select(SqlConnection sqlCon)
        {
            string queryString = prepereSelectQuery();
            SqlCommand cmd = new SqlCommand(queryString, sqlCon);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable(TableName);
            sda.Fill(dt);

            return dt;
        }

        public void Insert(SqlConnection sqlCon,DataWrapper dataWrapper)
        {
            if (LoadObject(DataWindows.Helper.WindowType.Insert,dataWrapper,sqlCon) == true)
            {
                string queryString = prepareInsertQuery();
                SqlCommand command = new SqlCommand(queryString, sqlCon);
                try
                {
                    command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
        }

        public void Update(SqlConnection sqlCon, DataWrapper dataWrapper)
        {
            if (LoadObject(DataWindows.Helper.WindowType.Update,dataWrapper,sqlCon) == true)
            {
                string queryString = prepareUpdateQuery();
                SqlCommand command = new SqlCommand(queryString, sqlCon);
                try
                {
                    command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
        }

        public void Delete(SqlConnection sqlCon, DataWrapper dataWrapper)
        {
            if (LoadObject(DataWindows.Helper.WindowType.Delete,dataWrapper,sqlCon) == true)
            {
                string queryString = prepareDeleteQuery();
                SqlCommand command = new SqlCommand(queryString, sqlCon);
                try
                {
                    command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
        }
    }
}
