﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sklep_z_oprogramowaniem.DataWrappers
{
    public class DataField<T>
    {
        private T fieldValue;

        public readonly string Name;
        public readonly string FriendlyName;
        public bool isUsed;
        
        public T Value
        {
            set
            {
                fieldValue = value;
                isUsed = true;
            }
            get
            {
                return fieldValue;
            }
        }

        public DataField(string fieldName, string friendlyName = "")
        {
            Name = fieldName;
            if (friendlyName == "")
                FriendlyName = Name;
            else
                FriendlyName = friendlyName;
        }
    }

    public class ContextField<T> : DataField<T>
    {
        public readonly string contextTableName;
        public readonly string contextFieldName;
        public readonly string contextFriendlyFieldName;

        public string ContextFieldValue { set; get; }

        public ContextField(string fieldName, string contextTable, string contextField,
            string contextFriendlyName)
            : base(fieldName)
        {
            contextTableName = contextTable;
            contextFieldName = contextField;
            contextFriendlyFieldName = contextFriendlyName;
        }
    }
}
