﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sklep_z_oprogramowaniem.DataWrappers
{
    static class Helper
    {
        public static string getASfield<T>(DataField<T> field,string tableName)
        {
            if (field.isUsed)
            {
                return string.Format(", {2}.{0} AS {1} ", field.Name, field.FriendlyName, tableName);
            }
            else
            {
                return string.Empty;
            }
        }

        public static string getASCOntextField<T>(ContextField<T> field)
        {
            if (field.isUsed)
            {
                return string.Format(" , {0}.{1} AS {2} ", field.contextTableName,
                    field.contextFieldName, field.contextFriendlyFieldName);
            }
            else
            {
                return string.Empty;
            }
        }

        public static string getJOIN<T>(ContextField<T> field, string tableName)
        {
            return string.Format(" INNER JOIN {0} ON {1}.{2} = {4}.{3} ", field.contextTableName,
                    field.contextTableName, Names.FieldNames.Id, field.Name, tableName);
        }

        public static string getWHEREId(DataField<long> Id)
        {
            return string.Format(" WHERE {0} = {1}", Id.Name, Id.Value);
        }

        public static string getSET<T>(DataField<T> field, bool isChar = false, bool addComma = true)
        {
            string query = string.Empty;
            if(field.isUsed)
            {
                if(!isChar)
                {
                    query = string.Format(" {0} = {1} ", field.Name, field.Value).Replace(',','.');
                }
                else
                {
                    query = string.Format(" {0} = '{1}' ", field.Name, field.Value);
                }
                if (addComma)
                {
                    query += " , ";
                }
            }
            return query;
            
        }

        public static string getSETnull<T>(DataField<T> field, bool addComma = true)
        {
            string query = string.Format(" {0} = NULL ", field.Name);
            if (addComma)
            {
                query += " , ";
            }
            return query;
        }

        public static string getINTOfieldNames(params string[] fields)
        {
            string query = "(";
            foreach (string field in fields)
                query += field + ",";
            return query.Substring(0, query.Length - 1) + ")";
        }

        public static string getWithQuote(string name)
        {
            return "'" + name + "'";
        }
    }
}
