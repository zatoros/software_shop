﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sklep_z_oprogramowaniem.DataWrappers
{
    public class DataWrapper
    {
        private ClientStatusWrapper ClientStatus = new ClientStatusWrapper();
        private ClientWrapper Client = new ClientWrapper();
        private PositionWrapper Position = new PositionWrapper();
        private TransactionStatusWrapper TransactionStatus = new TransactionStatusWrapper();
        private TransactionWrapper Transaction = new TransactionWrapper();
        private WareWrapper Ware = new WareWrapper();
        private WorkerWrapper Worker = new WorkerWrapper();
        private WorkTimeWrapper WorkTime = new WorkTimeWrapper();
        private LicenseWrapper Licence = new LicenseWrapper();
        
        private Dictionary<string, RowHelper> tables = new Dictionary<string, RowHelper>();

        List<RowHelper> categoryTables = new List<RowHelper>();

        public Dictionary<string, RowHelper> Tables
        {
            get
            {
                return tables;
            }
        }

        public List<RowHelper> IsCategory
        {
            get
            {
                return categoryTables;
            }
        }

        public DataWrapper()
        {
            tables.Add(Names.FriendlyTableNames.ClientStatus,ClientStatus);
            tables.Add(Names.FriendlyTableNames.WorkTime,WorkTime);
            tables.Add(Names.FriendlyTableNames.Position,Position);
            tables.Add(Names.FriendlyTableNames.TransactionStatus,TransactionStatus);
            categoryTables.AddRange(tables.Values);

            tables.Add(Names.FriendlyTableNames.Clients,Client);
            tables.Add(Names.FriendlyTableNames.Transactions,Transaction);
            tables.Add(Names.FriendlyTableNames.Licenses,Licence);
            tables.Add(Names.FriendlyTableNames.Workers,Worker);
            tables.Add(Names.FriendlyTableNames.Wares, Ware);
        }
    }
}
