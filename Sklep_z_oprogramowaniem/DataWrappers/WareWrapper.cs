﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Controls;
using System.Windows;

namespace Sklep_z_oprogramowaniem.DataWrappers
{
    public class WareWrapper : RowHelper
    {
        private const string tableName = Names.TableNames.Wares;
        private DataWindows.WareDataWindow DataWindow;

        public DataField<string> Name = new DataField<string>(Names.FieldNames.WareName,
            Names.FriendlyFieldNames.WareName);
        public DataField<decimal> BasePrice = new DataField<decimal>(Names.FieldNames.WarePrice,
            Names.FriendlyFieldNames.WarePrice);

        public override string prepareInsertQuery() 
        {
            if (anyFieldUsed())
            {
                string queryString = string.Format("INSERT INTO {0} ", tableName);

                queryString += Helper.getINTOfieldNames(Name.Name, BasePrice.Name);

                queryString += " VALUES ";

                queryString += Helper.getINTOfieldNames(Helper.getWithQuote(Name.Value), BasePrice.Value.ToString().Replace(',','.'));

                return queryString;
            }
            else
            {
                throw new Exception(Names.ExceptionNames.NoUsedFieldsInsert);
            }
        }

        public override string prepareUpdateQuery()
        {
            if (anyFieldUsed())
            {
                string queryString = string.Format("UPDATE {0} SET ", tableName);

                queryString += Helper.getSET(Name, true);
                queryString += Helper.getSET(BasePrice, false, false);

                queryString += Helper.getWHEREId(Id);

                return queryString;
            }
            else
            {
                throw new Exception(Names.ExceptionNames.NoUsedFieldsUpdate);
            }
        }

        public override bool anyFieldUsed()
        {
            return Name.isUsed || BasePrice.isUsed;
        }

        public override string prepereSelectQuery()
        {
            string queryString = string.Empty;

            if (anyFieldUsed())
            {
                queryString = string.Format("SELECT {2}.{0} AS {1} ", Id.Name, Id.FriendlyName,
                    tableName);

                queryString += Helper.getASfield(Name, tableName);
                queryString += Helper.getASfield(BasePrice, tableName);

                queryString += string.Format(" FROM {0}", TableName);
            }
            else
            {
                throw new Exception(Names.ExceptionNames.NoUsedFieldsSelect);
            }

            return queryString;
        }

        public override void useAllFields()
        {
            Name.isUsed = true;
            BasePrice.isUsed = true;
        }

        public override void unUseAllFields()
        {
            Name.isUsed = false;
            BasePrice.isUsed = false;
        }

        public override string TableName
        {
            get { return tableName; }
        }

        public override bool? LoadObject(DataWindows.Helper.WindowType operationType, 
            DataWrapper dataWrapper, SqlConnection sqlCon) 
        {
            DataWindow = new DataWindows.WareDataWindow(operationType, this);
            return DataWindow.ShowDialog(); 
        }

        public override void GetObjectFromRow(DataRowView dataRowView) 
        {
            Id.Value = long.Parse(dataRowView[0].ToString());

            Name.Value = dataRowView[1].ToString();
            BasePrice.Value = decimal.Parse(dataRowView[2].ToString());
        }
    }
}
