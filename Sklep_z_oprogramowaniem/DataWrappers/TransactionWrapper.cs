﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Controls;
using System.Windows;

namespace Sklep_z_oprogramowaniem.DataWrappers
{
    public class TransactionWrapper : RowHelper
    {
        private const string tableName = Names.TableNames.Transactions;

        private DataWindows.TransactionDataWindow DataWindow;

        public DataField<decimal> Price = new DataField<decimal>(Names.FieldNames.TransactionPrice,
            Names.FriendlyFieldNames.TransactionPrice);
        public DataField<string> Description = new DataField<string>(Names.FieldNames.TransactionDescription,
            Names.FriendlyFieldNames.TransactionDescription);

        public ContextField<long> RTransactionStatusID = new ContextField<long>("RTransactionStatusID", Names.TableNames.TransactionStatus,
            Names.FieldNames.TransactionStatus, Names.FriendlyFieldNames.TransactionStatus);
        public ContextField<long> RWareID = new ContextField<long>("RWareID", Names.TableNames.Wares,
            Names.FieldNames.WareName, Names.FriendlyFieldNames.WareName);
        public ContextField<long> RClientID = new ContextField<long>("RClientID", Names.TableNames.Clients,
            Names.FieldNames.ClientName, Names.FriendlyFieldNames.ClientName);

        public override bool anyFieldUsed()
        {
            return Price.isUsed || Description.isUsed || RTransactionStatusID.isUsed
                || RWareID.isUsed || RClientID.isUsed;
        }

        public override string prepareInsertQuery()
        {
            if (anyFieldUsed())
            {
                string queryString = string.Format("INSERT INTO {0} ", tableName);

                queryString += Helper.getINTOfieldNames(Price.Name, Description.Name,
                    RTransactionStatusID.Name, RWareID.Name, RClientID.Name);

                queryString += " VALUES ";

                queryString += Helper.getINTOfieldNames(Price.Value.ToString().Replace(',', '.'),
                    Description.Value != "" ? Helper.getWithQuote(Description.Value) : "NULL",
                    RTransactionStatusID.Value.ToString(), RWareID.Value.ToString(), RClientID.Value.ToString());

                return queryString;
            }
            else
            {
                throw new Exception(Names.ExceptionNames.NoUsedFieldsInsert);
            }
        }

        public override string prepareUpdateQuery()
        {
            if (anyFieldUsed())
            {
                string queryString = string.Format("UPDATE {0} SET ", tableName);

                queryString += Helper.getSET(Price);
                queryString += Description.Value != "" ? Helper.getSET(Description, true) : Helper.getSETnull(Description);
                queryString += Helper.getSET(RTransactionStatusID);
                queryString += Helper.getSET(RWareID);
                queryString += Helper.getSET(RClientID, false, false);

                queryString += Helper.getWHEREId(Id);

                return queryString;
            }
            else
            {
                throw new Exception(Names.ExceptionNames.NoUsedFieldsUpdate);
            }
        }

        public override string prepereSelectQuery()
        {
            string queryString = string.Empty;

            if (anyFieldUsed())
            {
                queryString = string.Format("SELECT {2}.{0} AS {1} ", Id.Name, Id.FriendlyName,
                    tableName);

                queryString += Helper.getASfield(Price, tableName);
                queryString += Helper.getASfield(Description, tableName);

                queryString += Helper.getASfield(RTransactionStatusID, tableName);
                queryString += Helper.getASfield(RWareID, tableName);
                queryString += Helper.getASfield(RClientID, tableName);

                queryString += Helper.getASCOntextField(RTransactionStatusID);
                queryString += Helper.getASCOntextField(RWareID);
                queryString += Helper.getASCOntextField(RClientID);

                queryString += string.Format(" FROM {0}", TableName);

                queryString += Helper.getJOIN(RTransactionStatusID, TableName);
                queryString += Helper.getJOIN(RWareID, TableName);
                queryString += Helper.getJOIN(RClientID, TableName);
            }
            else
            {
                throw new Exception(Names.ExceptionNames.NoUsedFieldsSelect);
            }

            return queryString;
        }

        public override void useAllFields()
        {
            Price.isUsed = true;
            Description.isUsed = true;

            RTransactionStatusID.isUsed = true;
            RWareID.isUsed = true;
            RClientID.isUsed = true;
        }

        public override void unUseAllFields()
        {
            Price.isUsed = false;
            Description.isUsed = false;

            RTransactionStatusID.isUsed = false;
            RWareID.isUsed = false;
            RClientID.isUsed = false;
        }

        public override string TableName
        {
            get { return tableName; }
        }

        public override bool? LoadObject(DataWindows.Helper.WindowType operationType,
            DataWrapper dataWrapper, SqlConnection sqlCon)
        {
            DataWindow = new DataWindows.TransactionDataWindow(operationType, this, dataWrapper, sqlCon);
            return DataWindow.ShowDialog();
        }

        public override void GetObjectFromRow(DataRowView dataRowView) 
        {
            Id.Value = long.Parse(dataRowView[0].ToString());

            Price.Value = decimal.Parse(dataRowView[1].ToString());
            Description.Value = dataRowView[2].ToString();

            RTransactionStatusID.Value = long.Parse(dataRowView[3].ToString());
            RWareID.Value = long.Parse(dataRowView[4].ToString());
            RClientID.Value = long.Parse(dataRowView[5].ToString());
        }

        public override void HideIdRows(DataGrid grid)
        {
            base.HideIdRows(grid);

            grid.Columns[3].Visibility = Visibility.Hidden;
            grid.Columns[4].Visibility = Visibility.Hidden;
            grid.Columns[5].Visibility = Visibility.Hidden;
        }
    }
}
