﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sklep_z_oprogramowaniem.DataWrappers
{
    class TransactionStatusWrapper : IdValueRow
    {
        private const string tableName = Names.TableNames.TransactionStatus;
        private DataField<string> statusDescription = new DataField<string>(Names.FieldNames.TransactionStatus,
            Names.FriendlyFieldNames.TransactionStatus);

        public override DataField<string> Value
        {
            get
            {
                return statusDescription;
            }
            set
            {
                statusDescription = value;
            }
        }

        public override string TableName
        {
            get { return tableName; }
        }
    }
}
