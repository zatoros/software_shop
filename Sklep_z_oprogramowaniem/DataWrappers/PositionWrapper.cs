﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sklep_z_oprogramowaniem.DataWrappers
{
    class PositionWrapper : IdValueRow
    {
        private const string tableName = Names.TableNames.Position;
        private DataField<string> positionName = new DataField<string>(Names.FieldNames.PositionName,
            Names.FriendlyFieldNames.PositionName);

        public override DataField<string> Value
        {
            get
            {
                return positionName;
            }
            set
            {
                positionName = value;
            }
        }

        public override string TableName
        {
            get { return tableName; }
        }
    }
}
