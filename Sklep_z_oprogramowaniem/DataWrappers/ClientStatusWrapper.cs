﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sklep_z_oprogramowaniem.DataWrappers
{
    class ClientStatusWrapper : IdValueRow
    {
        private const string tableName = Names.TableNames.ClientStatus;
        private DataField<string> clientStatus = new DataField<string>(Names.FieldNames.ClientStatus,
            Names.FriendlyFieldNames.ClientStatus);

        public override DataField<string> Value
        {
            get
            {
                return clientStatus;
            }
            set
            {
                clientStatus = value;
            }
        }

        public override string TableName
        {
            get { return tableName; }
        }
    }
}
