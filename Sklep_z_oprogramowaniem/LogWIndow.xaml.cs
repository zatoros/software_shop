﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Collections.Specialized;

namespace Sklep_z_oprogramowaniem
{
    /// <summary>
    /// Interaction logic for LogWIndow.xaml
    /// </summary>
    public partial class LogWIndow : Window
    {
        public Helper.UserTypes resultUser;

        public LogWIndow()
        {
            InitializeComponent();
        }

        private void ButtonLog_Click(object sender, RoutedEventArgs e)
        {
            NameValueCollection config = System.Configuration.ConfigurationSettings.AppSettings;
            
            string userName = TextBoxUser.Text;
            string password = PasswordBoxPassword.Password;

            if (userName == "" || password == "")
            {
                MessageBox.Show("Należy podać hasło i użytkownika");
                return;
            }

            string desiredpassword = string.Empty;

            string[] passUserValue = config[userName].Split('-');

            if (passUserValue.Length < 2)
            {
                MessageBox.Show("Błąd konfiguracji użytkownika spróuj zalogować się na innego");
                return;
            }

            desiredpassword = passUserValue[0];

            if(desiredpassword == null)
            {
                MessageBox.Show("Podany użytkownik nie istnieje");
                return;
            }

            if (desiredpassword != password)
            {
                MessageBox.Show("Podane hasło jest niesprawidłowe");
                return;
            }

            if (passUserValue[1] == "Admin")
            {
                resultUser = Helper.UserTypes.Admin;
            }
            else if (passUserValue[1] == "User")
            {
                resultUser = Helper.UserTypes.User;
            }

            DialogResult = true;
            Close();
        }

        private void ButtonCancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }
    }
}
