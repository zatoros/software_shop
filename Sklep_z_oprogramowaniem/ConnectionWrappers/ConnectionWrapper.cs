﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Sklep_z_oprogramowaniem.ConnectionWrappers
{
    class ConnectionWrapper
    {
        public bool isConnected
        {
            get
            {
                return (sqlConnection.State == System.Data.ConnectionState.Open);
            }
        }

        public string ConnecionStrning { set; get; }

        private SqlConnection sqlConnection = new SqlConnection();

        public SqlConnection SqlConnection
        {
            get
            {
                return sqlConnection;
            }
        }

        public void Connect()
        {
            sqlConnection.ConnectionString = ConnecionStrning;

            sqlConnection.Open();
        }

        public void DisConnect()
        {
            if(sqlConnection.State == System.Data.ConnectionState.Open)
                sqlConnection.Close();
        }
        
        ~ConnectionWrapper()
        {
            DisConnect();
        }
    }
}
