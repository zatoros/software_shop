﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using System.Data;
using System.Data.SqlClient;

namespace Sklep_z_oprogramowaniem.DataWindows
{
    /// <summary>
    /// Interaction logic for WorkerDataWindow.xaml
    /// </summary>
    public partial class WorkerDataWindow : Window
    {
        public DataWrappers.WorkerWrapper Output { set; get; }
        private Helper.WindowType operationType; 

        public WorkerDataWindow(Helper.WindowType operation, DataWrappers.WorkerWrapper target, DataWrappers.DataWrapper dataWrapper,
            SqlConnection sqlCon)
        {
            InitializeComponent();
            
            NameLabel.Content = Names.FriendlyFieldNames.WorkerName;
            SurnameLabel.Content = Names.FriendlyFieldNames.WorkerSurname;
            SalaryLabel.Content = Names.FriendlyFieldNames.WorkerSalary;
            AddressLabel.Content = Names.FriendlyFieldNames.WorkerAddress;
            PhoneLabel.Content = Names.FriendlyFieldNames.WorkerPhone;

            RWorkTimeLabel.Content = target.RWorkTimeID.contextFriendlyFieldName;
            RPositionLabel.Content = target.RPositionID.contextFriendlyFieldName;

            Helper.fillComboBox(RWorkTimeComboBox, dataWrapper.Tables[Names.FriendlyTableNames.WorkTime], sqlCon,
                target.RWorkTimeID.contextFriendlyFieldName);

            Helper.fillComboBox(RPositionComboBox, dataWrapper.Tables[Names.FriendlyTableNames.Position], sqlCon,
                target.RPositionID.contextFriendlyFieldName);

            switch (operation)
            {
                case Helper.WindowType.Delete:
                    if (target == null)
                    {
                        throw new Exception(Names.ExceptionNames.NoFieldToDelete);
                    }
                    OkButton.Content = "Usuń";
                    Title = "Usuń pracownika";
                    NameTextBox.Text = target.Name.Value;
                    SurnameTextBox.Text = target.Surname.Value;
                    SalaryTextBox.Text = target.Salary.Value.ToString();
                    AddressTextBox.Text = target.Address.Value;
                    PhoneTextBox.Text = target.Phone.Value;

                    Helper.selectComboBoxIitem(RWorkTimeComboBox, target.RWorkTimeID.Value);
                    Helper.selectComboBoxIitem(RPositionComboBox, target.RPositionID.Value);

                    NameTextBox.IsEnabled = false;
                    SurnameTextBox.IsEnabled = false;
                    SalaryTextBox.IsEnabled = false;
                    AddressTextBox.IsEnabled = false;
                    PhoneTextBox.IsEnabled = false;

                    RWorkTimeComboBox.IsEnabled = false;
                    RPositionComboBox.IsEnabled = false;
                    break;
                case Helper.WindowType.Insert:
                    OkButton.Content = "Dodaj";
                    Title = "Dodaj pracownika";
                    break;
                case Helper.WindowType.Update:
                    if (target == null)
                    {
                        throw new Exception(Names.ExceptionNames.NoFieldToUpdate);
                    }
                    OkButton.Content = "Aktualizuj";
                    Title = "Aktualizuj dane pracownika";
                    NameTextBox.Text = target.Name.Value;
                    SurnameTextBox.Text = target.Surname.Value;
                    SalaryTextBox.Text = target.Salary.Value.ToString();
                    AddressTextBox.Text = target.Address.Value;
                    PhoneTextBox.Text = target.Phone.Value;

                    Helper.selectComboBoxIitem(RWorkTimeComboBox, target.RWorkTimeID.Value);
                    Helper.selectComboBoxIitem(RPositionComboBox, target.RPositionID.Value);
                    break;
            }

            Output = target;
            operationType = operation;
        }

        private void OkButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                switch (operationType)
                {
                    case Helper.WindowType.Insert:
                    case Helper.WindowType.Update:
                        decimal salary;
                        if (NameTextBox.Text == "")
                            throw new Exception(Names.ExceptionNames.FieldIsNull(Output.Name));
                        if (SurnameTextBox.Text == "")
                            throw new Exception(Names.ExceptionNames.FieldIsNull(Output.Surname));
                        if (SalaryTextBox.Text == "")
                            throw new Exception(Names.ExceptionNames.FieldIsNull(Output.Salary));
                        if (!decimal.TryParse(SalaryTextBox.Text, out salary))
                            throw new Exception(Names.ExceptionNames.FieldIsNotNumber(Output.Salary));
                        if (PhoneTextBox.Text == "")
                            throw new Exception(Names.ExceptionNames.FieldIsNull(Output.Phone));
                        if (RWorkTimeComboBox.SelectedItem == null)
                            throw new Exception(Names.ExceptionNames.FieldIsNull(Output.RWorkTimeID));
                        if (RPositionComboBox.SelectedItem == null)
                            throw new Exception(Names.ExceptionNames.FieldIsNull(Output.RPositionID));

                        Output.Name.Value = NameTextBox.Text;
                        Output.Surname.Value = SurnameTextBox.Text;
                        Output.Salary.Value = salary;
                        Output.Address.Value = AddressTextBox.Text;
                        Output.Phone.Value = PhoneTextBox.Text;

                        Output.RWorkTimeID.Value = Helper.getIdFromCombo(RWorkTimeComboBox.SelectedItem);
                        Output.RPositionID.Value = Helper.getIdFromCombo(RPositionComboBox.SelectedItem);

                        break;
                }
                DialogResult = true;
                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Helper.ComboBox_SelectionChanged(sender, e);
        }
    }
}
