﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Sklep_z_oprogramowaniem.DataWindows
{
    /// <summary>
    /// Interaction logic for WareDataWindow.xaml
    /// </summary>
    public partial class WareDataWindow : Window
    {
        public DataWrappers.WareWrapper Output { set; get; }
        private Helper.WindowType operationType;

        public WareDataWindow(Helper.WindowType operation, DataWrappers.WareWrapper target = null)
        {
            InitializeComponent();

            NameLabel.Content = Names.FriendlyFieldNames.WareName;
            BasePriceLabel.Content = Names.FriendlyFieldNames.WarePrice;

            switch (operation)
            {
                case Helper.WindowType.Delete:
                    if (target == null)
                    {
                        throw new Exception(Names.ExceptionNames.NoFieldToDelete);
                    }
                    OkButton.Content = "Usuń";
                    Title = "Usuń towar";
                    NameTextBox.Text = target.Name.Value;
                    BasePriceTextBox.Text = target.BasePrice.Value.ToString();

                    NameTextBox.IsEnabled = false;
                    BasePriceTextBox.IsEnabled = false;
                    break;
                case Helper.WindowType.Insert:
                    OkButton.Content = "Dodaj";
                    Title = "Dodaj towar";
                    break;
                case Helper.WindowType.Update:
                    if (target == null)
                    {
                        throw new Exception(Names.ExceptionNames.NoFieldToUpdate);
                    }
                    OkButton.Content = "Aktualizuj";
                    Title = "Aktualizuj dane towaru";
                    NameTextBox.Text = target.Name.Value;
                    BasePriceTextBox.Text = target.BasePrice.Value.ToString();
                    break;
            }

            Output = target;
            operationType = operation;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }

        private void OkButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                switch (operationType)
                {
                    case Helper.WindowType.Insert:
                    case Helper.WindowType.Update:
                        decimal price;
                        if (NameTextBox.Text == "")
                            throw new Exception(Names.ExceptionNames.FieldIsNull(Output.Name));
                        if (BasePriceTextBox.Text == "")
                            throw new Exception(Names.ExceptionNames.FieldIsNull(Output.BasePrice));
                        if (!decimal.TryParse(BasePriceTextBox.Text, out price))
                            throw new Exception(Names.ExceptionNames.FieldIsNotNumber(Output.BasePrice));

                        Output.Name.Value = NameTextBox.Text;
                        Output.BasePrice.Value = price;

                        break;
                }
                DialogResult = true;
                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
