﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Data;

namespace Sklep_z_oprogramowaniem.DataWindows
{
    /// <summary>
    /// Interaction logic for ClientDataWindow.xaml
    /// </summary>
    public partial class ClientDataWindow : Window
    {
        public DataWrappers.ClientWrapper Output { set; get; }
        private Helper.WindowType operationType; 

        public ClientDataWindow(Helper.WindowType operation, DataWrappers.ClientWrapper target, DataWrappers.DataWrapper dataWrapper,
            SqlConnection sqlCon)
        {
            InitializeComponent();

            NameLabel.Content = Names.FriendlyFieldNames.ClientName;
            PhoneLabel.Content = Names.FriendlyFieldNames.ClientPhone;
            AddressLabel.Content = Names.FriendlyFieldNames.ClientAddress;

            RClientStatusLabel.Content = target.RClientStatusID.contextFriendlyFieldName;
            RWorkerLabel.Content = target.RWorkerID.contextFriendlyFieldName;

            Helper.fillComboBox(RClientStatusComboBox, dataWrapper.Tables[Names.FriendlyTableNames.ClientStatus], sqlCon,
                target.RClientStatusID.contextFriendlyFieldName, true);

            Helper.fillComboBox(RWorkerComboBox, dataWrapper.Tables[Names.FriendlyTableNames.Workers], sqlCon,
                target.RWorkerID.contextFriendlyFieldName, true);

            switch (operation)
            {
                case Helper.WindowType.Delete:
                    if (target == null)
                    {
                        throw new Exception(Names.ExceptionNames.NoFieldToDelete);
                    }
                    OkButton.Content = "Usuń";
                    Title = "Usuń klienta";
                    NameTextBox.Text = target.Name.Value;
                    PhoneTextBox.Text = target.Phone.Value;
                    AddressTextBox.Text = target.Address.Value;
                    Helper.selectComboBoxIitem(RClientStatusComboBox, target.RClientStatusID.Value);
                    Helper.selectComboBoxIitem(RWorkerComboBox, target.RWorkerID.Value);

                    NameTextBox.IsEnabled = false;
                    PhoneTextBox.IsEnabled = false;
                    AddressTextBox.IsEnabled = false;
                    RClientStatusComboBox.IsEnabled = false;
                    RWorkerComboBox.IsEnabled = false;
                    break;
                case Helper.WindowType.Insert:
                    OkButton.Content = "Dodaj";
                    Title = "Dodaj klienta";
                    break;
                case Helper.WindowType.Update:
                    if (target == null)
                    {
                        throw new Exception(Names.ExceptionNames.NoFieldToUpdate);
                    }
                    OkButton.Content = "Aktualizuj";
                    Title = "Aktualizuj dane klienta";
                    NameTextBox.Text = target.Name.Value;
                    PhoneTextBox.Text = target.Phone.Value;
                    AddressTextBox.Text = target.Address.Value;
                    Helper.selectComboBoxIitem(RClientStatusComboBox, target.RClientStatusID.Value);
                    Helper.selectComboBoxIitem(RWorkerComboBox, target.RWorkerID.Value);
                    break;
            }

            Output = target;
            operationType = operation;
        }

        private void OkButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                switch (operationType)
                {
                    case Helper.WindowType.Insert:
                    case Helper.WindowType.Update:
                        Output.Name.Value = NameTextBox.Text.Trim();
                        Output.Phone.Value = PhoneTextBox.Text.Trim();
                        Output.Address.Value = AddressTextBox.Text.Trim();
                        Output.RClientStatusID.Value = Helper.getIdFromCombo(RClientStatusComboBox.SelectedItem);
                        Output.RWorkerID.Value = Helper.getIdFromCombo(RWorkerComboBox.SelectedItem);

                        if (Output.Name.Value == "")
                            throw new Exception(Names.ExceptionNames.FieldIsNull(Output.Name));
                        if (Output.Phone.Value == "")
                            throw new Exception(Names.ExceptionNames.FieldIsNull(Output.Phone));
                        break;
                }
                DialogResult = true;
                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Helper.ComboBox_SelectionChanged(sender, e);
        }
    }
}
