﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sklep_z_oprogramowaniem.DataWindows
{
    class ComboBoxDataField
    {
        public long Id { set; get; }
        public string FieldName { set; get; }

        public override string ToString()
        {
            return FieldName;
        }

        public ComboBoxDataField(long id, string fieldName)
        {
            Id = id;
            FieldName = fieldName;
        }
    }
}
