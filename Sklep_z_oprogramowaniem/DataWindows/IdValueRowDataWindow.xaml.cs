﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Sklep_z_oprogramowaniem.DataWindows
{
    /// <summary>
    /// Interaction logic for IdValueRowDataWindow.xaml
    /// </summary>
    public partial class IdValueRowDataWindow : Window
    {
        public DataWrappers.IdValueRow Output { set; get; }
        private Helper.WindowType operationType;

        public IdValueRowDataWindow(Helper.WindowType operation,DataWrappers.IdValueRow target = null)
        {
            InitializeComponent();
            switch(operation)
            {
                case Helper.WindowType.Delete:
                    if (target == null)
                    {
                        throw new Exception(Names.ExceptionNames.NoFieldToDelete);
                    }
                    OkButton.Content = "Usuń";
                    InputTextBox.Text = target.Value.Value;
                    InputTextBox.IsEnabled = false;
                    Title = "Usuń wiersz z tabeli ";
                    break;
                case Helper.WindowType.Insert:
                    OkButton.Content = "Dodaj";
                    Title = "Dodaj wiersz do tabeli ";
                    break;
                case Helper.WindowType.Update:
                    if (target == null)
                    {
                        throw new Exception(Names.ExceptionNames.NoFieldToUpdate);
                    }
                    OkButton.Content = "Aktualizuj";
                    InputTextBox.Text = target.Value.Value;
                    InputTextBox.IsEnabled = true;
                    Title = "Aktualizuj wiersz w tabeli ";
                    break;
            }
            Title += target.TableName; 
            LabellFieldInfo.Content = target.Value.FriendlyName;
            Output = target;
            operationType = operation;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }

        private void OkButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string inputText = InputTextBox.Text.Trim();
                switch (operationType)
                {
                    case Helper.WindowType.Insert:
                    case Helper.WindowType.Update:
                        if (inputText == string.Empty)
                        {
                            throw new Exception(Names.ExceptionNames.FieldIsNull(Output.Value));
                        }
                        Output.Value.Value = inputText;
                        break;
                }
                DialogResult = true;
                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
