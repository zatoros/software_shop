﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using System.Data;
using System.Data.SqlClient;

namespace Sklep_z_oprogramowaniem.DataWindows
{
    /// <summary>
    /// Interaction logic for LicenseDataWindow.xaml
    /// </summary>
    public partial class LicenseDataWindow : Window
    {
        public DataWrappers.LicenseWrapper Output { set; get; }
        private Helper.WindowType operationType; 

        public LicenseDataWindow(Helper.WindowType operation, DataWrappers.LicenseWrapper target, DataWrappers.DataWrapper dataWrapper,
            SqlConnection sqlCon)
        {
            InitializeComponent();

            DateStartLabel.Content = Names.FriendlyFieldNames.LicenceStart;
            DateEndLabel.Content = Names.FriendlyFieldNames.LicenceEnd;

            RTransactionLabel.Content = target.RTransactionID.contextFriendlyFieldName;

            Helper.fillComboBox(RTransactionComboBox, dataWrapper.Tables[Names.FriendlyTableNames.Transactions], sqlCon,
                target.RTransactionID.contextFriendlyFieldName);

            switch (operation)
            {
                case Helper.WindowType.Delete:
                    if (target == null)
                    {
                        throw new Exception(Names.ExceptionNames.NoFieldToDelete);
                    }
                    OkButton.Content = "Usuń";
                    Title = "Usuń licencje";
                    DateStartPicker.SelectedDate = target.StartDate.Value;
                    DateEndPicker.SelectedDate = target.EndDate.Value;
                    Helper.selectComboBoxIitem(RTransactionComboBox, target.RTransactionID.Value);

                    DateStartPicker.IsEnabled = false;
                    DateEndPicker.IsEnabled = false;
                    RTransactionComboBox.IsEnabled = false;
                    break;
                case Helper.WindowType.Insert:
                    OkButton.Content = "Dodaj";
                    Title = "Dodaj licencje";
                    break;
                case Helper.WindowType.Update:
                    if (target == null)
                    {
                        throw new Exception(Names.ExceptionNames.NoFieldToUpdate);
                    }
                    OkButton.Content = "Aktualizuj";
                    Title = "Aktualizuj dane licencji";
                    DateStartPicker.SelectedDate = target.StartDate.Value;
                    DateEndPicker.SelectedDate = target.EndDate.Value;
                    Helper.selectComboBoxIitem(RTransactionComboBox, target.RTransactionID.Value);
                    break;
            }

            Output = target;
            operationType = operation;
        }

        private void RTransactionComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Helper.ComboBox_SelectionChanged(sender, e);
        }

        private void OkButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                switch (operationType)
                {
                    case Helper.WindowType.Insert:
                    case Helper.WindowType.Update:
                        if (DateStartPicker.SelectedDate == null)
                            throw new Exception(Names.ExceptionNames.FieldIsNull(Output.StartDate));
                        if (DateEndPicker.SelectedDate == null)
                            throw new Exception(Names.ExceptionNames.FieldIsNull(Output.EndDate));
                        if(RTransactionComboBox.SelectedItem == null)
                            throw new Exception(Names.ExceptionNames.FieldIsNull(Output.RTransactionID));

                        Output.StartDate.Value = DateStartPicker.SelectedDate ?? DateTime.Now;
                        Output.EndDate.Value = DateEndPicker.SelectedDate ?? DateTime.Now;
                        Output.RTransactionID.Value = Helper.getIdFromCombo(RTransactionComboBox.SelectedItem);
                        break;
                }
                DialogResult = true;
                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }
    }
}
