﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using System.Data;
using System.Data.SqlClient;

namespace Sklep_z_oprogramowaniem.DataWindows
{
    /// <summary>
    /// Interaction logic for TransactionDataWindow.xaml
    /// </summary>
    public partial class TransactionDataWindow : Window
    {
        public DataWrappers.TransactionWrapper Output { set; get; }
        private Helper.WindowType operationType; 

        public TransactionDataWindow(Helper.WindowType operation, DataWrappers.TransactionWrapper target, DataWrappers.DataWrapper dataWrapper,
            SqlConnection sqlCon)
        {
            InitializeComponent();

            PriceLabel.Content = Names.FriendlyFieldNames.TransactionPrice;
            DescriptionLabel.Content = Names.FriendlyFieldNames.TransactionDescription;

            RTransactionStatusLabel.Content = target.RTransactionStatusID.contextFriendlyFieldName;
            RWareLabel.Content = target.RWareID.contextFriendlyFieldName;
            RClientLabel.Content = target.RClientID.contextFriendlyFieldName;

            Helper.fillComboBox(RTransactionStatusComboBox, 
                dataWrapper.Tables[Names.FriendlyTableNames.TransactionStatus], sqlCon,
                target.RTransactionStatusID.contextFriendlyFieldName);

            Helper.fillComboBox(RWareComboBox,
                dataWrapper.Tables[Names.FriendlyTableNames.Wares], sqlCon,
                target.RWareID.contextFriendlyFieldName);

            Helper.fillComboBox(RClientComboBox,
                 dataWrapper.Tables[Names.FriendlyTableNames.Clients], sqlCon,
                 target.RClientID.contextFriendlyFieldName);

            switch (operation)
            {
                case Helper.WindowType.Delete:
                    if (target == null)
                    {
                        throw new Exception(Names.ExceptionNames.NoFieldToDelete);
                    }
                    OkButton.Content = "Usuń";
                    Title = "Usuń tranzakcje";
                    PriceTextBox.Text = target.Price.Value.ToString();
                    DescriptionTextBox.Text = target.Description.Value;

                    Helper.selectComboBoxIitem(RTransactionStatusComboBox, target.RTransactionStatusID.Value);
                    Helper.selectComboBoxIitem(RWareComboBox, target.RWareID.Value);
                    Helper.selectComboBoxIitem(RClientComboBox, target.RClientID.Value);

                    PriceTextBox.IsEnabled = false;
                    DescriptionTextBox.IsEnabled = false;
                    RTransactionStatusComboBox.IsEnabled = false;
                    RWareComboBox.IsEnabled = false;
                    RClientComboBox.IsEnabled = false;
                    break;
                case Helper.WindowType.Insert:
                    OkButton.Content = "Dodaj";
                    Title = "Dodaj tranzakcje";
                    break;
                case Helper.WindowType.Update:
                    if (target == null)
                    {
                        throw new Exception(Names.ExceptionNames.NoFieldToUpdate);
                    }
                    OkButton.Content = "Aktualizuj";
                    Title = "Aktualizuj dane tranzakcji";
                    PriceTextBox.Text = target.Price.Value.ToString();
                    DescriptionTextBox.Text = target.Description.Value;
                    Helper.selectComboBoxIitem(RTransactionStatusComboBox, target.RTransactionStatusID.Value);
                    Helper.selectComboBoxIitem(RWareComboBox, target.RWareID.Value);
                    Helper.selectComboBoxIitem(RClientComboBox, target.RClientID.Value);
                    break;
            }

            Output = target;
            operationType = operation;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }

        private void OkButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                switch (operationType)
                {
                    case Helper.WindowType.Insert:
                    case Helper.WindowType.Update:
                        decimal price;
                        if (PriceTextBox.Text == "")
                            throw new Exception(Names.ExceptionNames.FieldIsNull(Output.Price));
                        if(!decimal.TryParse(PriceTextBox.Text, out price))
                            throw new Exception(Names.ExceptionNames.FieldIsNotNumber(Output.Price));
                        if(RTransactionStatusComboBox.SelectedItem == null)
                            throw new Exception(Names.ExceptionNames.FieldIsNull(Output.RTransactionStatusID));
                        if (RWareComboBox.SelectedItem == null)
                            throw new Exception(Names.ExceptionNames.FieldIsNull(Output.RWareID));
                        if (RClientComboBox.SelectedItem == null)
                            throw new Exception(Names.ExceptionNames.FieldIsNull(Output.RClientID));

                        Output.Price.Value = price;
                        Output.Description.Value = DescriptionTextBox.Text;
                        Output.RTransactionStatusID.Value = Helper.getIdFromCombo(RTransactionStatusComboBox.SelectedItem);
                        Output.RWareID.Value = Helper.getIdFromCombo(RWareComboBox.SelectedItem);
                        Output.RClientID.Value = Helper.getIdFromCombo(RClientComboBox.SelectedItem);

                        break;
                }
                DialogResult = true;
                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Helper.ComboBox_SelectionChanged(sender, e);
        }
    }
}
