﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

using System.Data;
using System.Data.SqlClient;

namespace Sklep_z_oprogramowaniem.DataWindows
{
    public static class Helper
    {
        public enum WindowType { Update, Delete, Insert };

        static public void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox comboBox = (ComboBox)sender;
            ComboBoxDataField comboBoxItem = comboBox.SelectedItem as ComboBoxDataField;
            if (comboBoxItem == null || comboBoxItem.FieldName == "")
                comboBox.SelectedItem = null;
        }

        static public void fillComboBox(ComboBox comboBox, DataWrappers.RowHelper rowHelper, SqlConnection sqlCon, 
            string fieldName,
            bool acceptNulls = false)
        {
            rowHelper.useAllFields();
            DataTable dt = rowHelper.Select(sqlCon);
            rowHelper.unUseAllFields();
            foreach (DataRow row in dt.Rows)
                comboBox.Items.Add( new ComboBoxDataField((long)row[Names.FriendlyFieldNames.Id],
                    (string)row[fieldName]));
            if (acceptNulls)
                comboBox.Items.Add(new ComboBoxDataField(0, ""));
        }

        static public long getIdFromCombo(object comboItem)
        {
            if (comboItem == null)
                return 0;
            return ((ComboBoxDataField)comboItem).Id;
        }

        static public void selectComboBoxIitem(ComboBox comboBox, long id)
        {
            for (int i = 0; i < comboBox.Items.Count; i++)
            {
                if (((ComboBoxDataField)(comboBox.Items[i])).Id == id)
                {
                    comboBox.SelectedIndex = i;
                    break;
                }
            }
        }
    }
}
