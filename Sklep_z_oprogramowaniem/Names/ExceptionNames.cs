﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sklep_z_oprogramowaniem.Names
{
    public static class ExceptionNames
    {
        public static string NoUsedFieldsInsert = "Błąd przy dodawaniu rekordów:" +
                    "przynajmniej jedno pole musi być aktywne";
        public static string NoUsedFieldsUpdate = "Błąd przy aktualizacji rekordów:" +
                    "przynajmniej jedno pole musi być aktywne";
        public static string NoUsedFieldsDelete = "Błąd przy usuwaniu rekordu:" +
                    "przynajmniej jedno pole musi być aktywne";
        public static string NoUsedFieldsSelect = "Błąd przy ładowaniu rekordów:" +
                    "przynajmniej jedno pole musi być aktywne";
        public static string NoFieldToUpdate = "Nie podano obiektu do aktualizacji";
        public static string NoFieldToDelete = "Nie podano obiektu do usunięcia";

        public static string FieldIsNull<T>(DataWrappers.DataField<T> field)
        {
            return string.Format("Pole {0} nie może być puste", field.FriendlyName);
        }

        public static string FieldIsNotNumber<T>(DataWrappers.DataField<T> field)
        {
            return string.Format("Pole {0} jest polem numerycznym", field.FriendlyName);
        }
    }
}
