﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sklep_z_oprogramowaniem.Names
{
    public static class FieldNames
    {
        #region Id_Field

        public const string Id = "ID";

        #endregion

        public const string WorkTimeType = "WorkTimeType";
        public const string WorkerName = "Name";
        public const string WorkerSurname = "Surname";
        public const string WorkerSalary = "Salary";
        public const string WorkerAddress = "Address";
        public const string WorkerPhone = "Phone";

        public const string PositionName = "PositionName";

        public const string ClientName = "Name";
        public const string ClientPhone = "Phone";
        public const string ClientAddress = "Address";

        public const string ClientStatus = "StatusName";

        public const string LicenceStart = "StartDate";
        public const string LicenceEnd = "EndDate";

        public const string TransactionPrice = "Price";
        public const string TransactionDescription = "Description";

        public const string TransactionStatus = "StatusDescription";

        public const string WareName = "Name";
        public const string WarePrice = "BasePrice";
    }

    public static class FriendlyFieldNames
    {
        #region Id_Field

        public const string Id = "id";

        #endregion

        public const string WorkTimeType = "TrybPracy";
        public const string WorkerName = "ImięPracownika";
        public const string WorkerSurname = "NazwiskoPracownika";
        public const string WorkerSalary = "Pensja";
        public const string WorkerAddress = "Adres";
        public const string WorkerPhone = "Telefon";

        public const string PositionName = "Stanowisko";

        public const string ClientName = "ImięKlienta";
        public const string ClientPhone = "Telefon";
        public const string ClientAddress = "Adres";

        public const string ClientStatus = "Status";

        public const string LicenceStart = "Początek";
        public const string LicenceEnd = "Koniec";

        public const string TransactionPrice = "Wartość";
        public const string TransactionDescription = "Opis";

        public const string TransactionStatus = "Status";

        public const string WareName = "NazwaTowaru";
        public const string WarePrice = "CenaBazowa";
    }
}
