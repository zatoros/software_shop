﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sklep_z_oprogramowaniem.Names
{
    public static class TableNames
    {
        public const string ClientStatus = "ClientStatuses";
        public const string WorkTime = "WorkTime";
        public const string Position = "Positions";
        public const string TransactionStatus = "TransactionStatuses";

        public const string Clients = "Clients";
        public const string Transactions = "Transactions";
        public const string Licenses = "Licences";
        public const string Workers = "Workers";
        public const string Wares = "Wares";
    }

    public static class FriendlyTableNames
    {
        public const string ClientStatus = "Statusy klientów";
        public const string WorkTime = "Tryby pracy";
        public const string Position = "Stanowiska";
        public const string TransactionStatus = "Statusy tranzakcji";

        public const string Clients = "Klienci";
        public const string Transactions = "Tranzakcje";
        public const string Licenses = "Licencje";
        public const string Workers = "Pracownicy";
        public const string Wares = "Towary";
    }
}
