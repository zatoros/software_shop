﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Microsoft.Data.ConnectionUI;

using System.Data;

namespace Sklep_z_oprogramowaniem
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ConnectionWrappers.ConnectionWrapper connection = new ConnectionWrappers.ConnectionWrapper();

        DataWrappers.DataWrapper dataTables = new DataWrappers.DataWrapper();

        Helper.UserTypes user = Helper.UserTypes.NotLogged;

        string selectedTable = string.Empty;

        public MainWindow()
        {
            InitializeComponent();

            foreach (string tableNames in dataTables.Tables.Keys)
            {
                if (dataTables.IsCategory.Contains(dataTables.Tables[tableNames]))
                    ComboBoxCategoryTables.Items.Add(tableNames);
                else
                    ComboBoxTables.Items.Add(tableNames);
            }
            ComboBoxTables.SelectedIndex = 0;
            ComboBoxCategoryTables.SelectedIndex = 0;
            CheckConnection();
            CheckUser();
        }

        private void fillStatusLabel()
        {
            string status = string.Empty;

            if (connection.isConnected)
            {
                status += "Połączony z bazą danych";
            }
            else
            {
                status += "Brak połączenia z bazą danych";
            }

            status += " ; Użytkownik:";

            switch (user)
            {
                case Helper.UserTypes.NotLogged:
                    status += "Niezalogowany";
                    break;
                case Helper.UserTypes.Admin:
                    status += "Administrator";
                    break;
                case Helper.UserTypes.User:
                    status += "Użytkownik";
                    break;
            }
            StatusLabel.Content = status;
        }

        private void CheckConnection()
        {
            bool connected = connection.isConnected;

            MenuItemConnect.IsEnabled = !connected;
            MeuItemUnConnect.IsEnabled = connected;

            if (connected)
            {
                RectangleStatus.Fill = new SolidColorBrush(Colors.Gray);
            }
            else
            {
                RectangleStatus.Fill = new SolidColorBrush(Colors.Red);
            }

            fillStatusLabel();
        }

        private void CheckUser()
        {
            bool isNotLogged = (user == Helper.UserTypes.NotLogged);

            MenuItemLogin.IsEnabled = isNotLogged;
            MenuItemUnlog.IsEnabled = !isNotLogged;

            ButtonInsert.IsEnabled = !isNotLogged;
            ButtonUpdate.IsEnabled = !isNotLogged;
            ButtonDelete.IsEnabled = !isNotLogged;

            ContextMenuInsert.IsEnabled = !isNotLogged;
            ContextMenuRemove.IsEnabled = !isNotLogged;
            ContextMenuUpdate.IsEnabled = !isNotLogged;

            bool isAdmin = (user == Helper.UserTypes.Admin);

            if (isAdmin)
            {
                LabelCategoryTable.Visibility = System.Windows.Visibility.Visible;
                ButtonShowCategoryTable.Visibility = System.Windows.Visibility.Visible;
                ComboBoxCategoryTables.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                LabelCategoryTable.Visibility = System.Windows.Visibility.Hidden;
                ButtonShowCategoryTable.Visibility = System.Windows.Visibility.Hidden;
                ComboBoxCategoryTables.Visibility = System.Windows.Visibility.Hidden;
            }

            fillStatusLabel();
        }

        private void MenuItemConnect_Click(object sender, RoutedEventArgs e)
        {
            DataConnectionDialog dcd = new DataConnectionDialog();

            if (DataConnectionDialog.Show(dcd) == System.Windows.Forms.DialogResult.OK)
            {
                MessageBox.Show(dcd.ConnectionString);
                connection.ConnecionStrning = dcd.ConnectionString;
                connection.Connect();
            }
            CheckConnection();
        }

        private void MeuItemUnConnect_Click(object sender, RoutedEventArgs e)
        {
            connection.DisConnect();

            CheckConnection();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            connection.DisConnect();
        }

        private void LoadTable()
        {
            DataWrappers.RowHelper rowHelper = dataTables.Tables[selectedTable];
            rowHelper.useAllFields();

            DataTable dt = rowHelper.Select(connection.SqlConnection);
            dt.TableName = selectedTable;

            MainGrid.ItemsSource = dt.DefaultView;
            rowHelper.HideIdRows(MainGrid);

            rowHelper.unUseAllFields();
        }

        private void ButtonShowTable_Click(object sender, RoutedEventArgs e)
        {
            if (connection.isConnected)
            {
                selectedTable = ComboBoxTables.SelectedItem.ToString();
                LoadTable();
            }
            else
            {
                MessageBox.Show("Brak połączenia z bazą danych");
            }
        }

        private void ButtonShowCategoryTable_Click(object sender, RoutedEventArgs e)
        {
            if (connection.isConnected)
            {
                selectedTable = ComboBoxCategoryTables.SelectedItem.ToString();
                LoadTable();
            }
            else
            {
                MessageBox.Show("Brak połączenia z bazą danych");
            }
        }

        private void ButtonInsert_Click(object sender, RoutedEventArgs e)
        {
            if (checkSelectedTable())
            {
                DataWrappers.RowHelper rowHelper = dataTables.Tables[selectedTable];

                rowHelper.Insert(connection.SqlConnection, dataTables);

                LoadTable();
            }
            else
            {
                MessageBox.Show("Żeby zacząć edycje należy najpierw pokazać tabelę");
            }
        }

        private void ButtonDelete_Click(object sender, RoutedEventArgs e)
        {
            if (checkSelectedTable())
            {
                DataWrappers.RowHelper rowHelper = dataTables.Tables[selectedTable];

                DataRowView o = getSelectedRow();
                if (o == null)
                {
                    MessageBox.Show("Do wykonania operacji należy najpierw zaznaczyć wiersz");
                    return;
                }
                rowHelper.GetObjectFromRow(o);
                rowHelper.Delete(connection.SqlConnection, dataTables);

                LoadTable();
            }
            else
            {
                MessageBox.Show("Żeby zacząć edycje należy najpierw pokazać tabelę");
            }
        }

        private void ButtonUpdate_Click(object sender, RoutedEventArgs e)
        {
            if (checkSelectedTable())
            {
                DataWrappers.RowHelper rowHelper = dataTables.Tables[selectedTable];
                
                DataRowView o = getSelectedRow();
                if (o == null)
                {
                    MessageBox.Show("Do wykonania operacji należy najpierw zaznaczyć wiersz");
                    return;
                }
                rowHelper.GetObjectFromRow(o);
                rowHelper.Update(connection.SqlConnection, dataTables);

                LoadTable();
            }
            else
            {
                MessageBox.Show("Żeby zacząć edycje należy najpierw pokazać tabelę");
            }
        }

        private bool checkSelectedTable()
        {
            if (selectedTable == string.Empty)
            {
                return false;
            }
            return true;
        }

        private DataRowView getSelectedRow()
        {
            if (MainGrid.SelectedIndex != -1)
            {
                return ((DataView)(MainGrid.ItemsSource))[MainGrid.SelectedIndex];
            }
            else
            {
                return null;
            }
        }

        private void MenuItemLogin_Click(object sender, RoutedEventArgs e)
        {
            LogWIndow logWindow = new LogWIndow();

            if (logWindow.ShowDialog() == true)
            {
                user = logWindow.resultUser;
            }

            CheckUser();
        }

        private void MenuItemUnlog_Click(object sender, RoutedEventArgs e)
        {
            user = Helper.UserTypes.NotLogged;
            CheckUser();
        }

        private void ContextMenuRemove_Click(object sender, RoutedEventArgs e)
        {
            ButtonDelete_Click(sender, e);
        }

        private void ContextMenuUpdate_Click(object sender, RoutedEventArgs e)
        {
            ButtonUpdate_Click(sender, e);
        }

        private void ContextMenuInsert_Click(object sender, RoutedEventArgs e)
        {
            ButtonInsert_Click(sender, e);
        }

        private void MainGrid_ContextMenuOpening(object sender, ContextMenuEventArgs e)
        {
            ContextMenuInsert.IsEnabled = checkSelectedTable();
            
            bool isSelectedRow = (getSelectedRow() == null);
            
            ContextMenuUpdate.IsEnabled = !isSelectedRow;
            ContextMenuRemove.IsEnabled = !isSelectedRow;

            CheckUser();
        }
    }
}
